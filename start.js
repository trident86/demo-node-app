require('dotenv').config();

/*
const MongoClient = require('mongodb').MongoClient;
console.log("DB=" + process.env.DATABASE);
const client = new MongoClient('mongodb://trident:<PASSWORD>@mycluster-shard-00-00-tremv.mongodb.net:27017,mycluster-shard-00-01-tremv.mongodb.net:27017,mycluster-shard-00-02-tremv.mongodb.net:27017/test?ssl=true&replicaSet=MyCluster-shard-0&authSource=admin&retryWrites=true', { useNewUrlParser: true });
client.connect(err => {
  const collection = client.db("sp-node-article").collection("registrations");
 // perform actions on the collection object
  client.close();
});
*/

const mongoose = require('mongoose');

mongoose.connect(process.env.DATABASE, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
mongoose.connection
  .on('connected', () => {
    console.log(`Mongoose connection open on ${process.env.DATABASE}`);
  })
  .on('error', (err) => {
    console.log(`Connection error: ${err.message}`);
  });

require('./models/Registration');
const app = require('./app');

const server = app.listen(3000, () => {
  console.log(`Express is running on port ${server.address().port}`);
});